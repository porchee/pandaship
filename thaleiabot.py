# -*- coding: utf-8 -*-
# @Author: Marylette Roa
# @Date:   2017-11-25 02:38:19
# @Last Modified by:   Marylette Roa
# @Last Modified time: 2017-11-25 05:05:16
# code for diagflow python 


import apiai
import json
# from flask import Flask, request
import requests

from bottle import request, run, route, template, static_file, request, post, redirect

CLIENT_ACCESS_TOKEN = 'ee66b3bede244c798d096f5906f45cd6'


@route('/')
def index():
    return(template('index'))


@post('/')
def get_message():
    message = request.forms.get('message')
    return(redirect(f'/{message}'))


@route('/<message>')
def bot(message):
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    user_message = message

    request = ai.text_request()
    request.query = user_message

    response = json.loads(request.getresponse().read())

    # print(response['result']['fulfillment']['speech'])
    # return(response['result']['fulfillment']['speech'])
    return(response)



if __name__ == '__main__':
    run(debug=True, reloader=True, port=5000)